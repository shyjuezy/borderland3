import React from 'react';
import './App.scss';
import {
  useTable,
  usePagination,
  useSortBy,
  useFilters,
  useGroupBy,
  useExpanded,
  useRowSelect,
} from 'react-table'
import matchSorter from 'match-sorter'
import JsonData from './data/data.json'

// Define a default UI for filtering
function DefaultColumnFilter({
  column: { filterValue, preFilteredRows, setFilter },
}) {
  const count = preFilteredRows.length

  return (
    <input
      value={filterValue || ''}
      onChange={e => {
        setFilter(e.target.value || undefined) // Set undefined to remove the filter entirely
      }}
      placeholder={`Search ${count} records...`}
    />
  )
}

function fuzzyTextFilterFn(rows, id, filterValue) {
  return matchSorter(rows, filterValue, { keys: [row => row.values[id]] })
}

// Let the table remove the filter if the string is empty
fuzzyTextFilterFn.autoRemove = val => !val

// Be sure to pass options
function Table({ columns, data, skipReset }) {
  const filterTypes = React.useMemo(
    () => ({
      // Add a new fuzzyTextFilterFn filter type.
      fuzzyText: fuzzyTextFilterFn,
      // Or, override the default text filter to use
      // "startWith"
      text: (rows, id, filterValue) => {
        return rows.filter(row => {
          const rowValue = row.values[id]
          return rowValue !== undefined
            ? String(rowValue)
              .toLowerCase()
              .startsWith(String(filterValue).toLowerCase())
            : true
        })
      },
    }),
    []
  )

  const defaultColumn = React.useMemo(
    () => ({
      Filter: DefaultColumnFilter,
    }),
    []
  )

  // Use the state and functions returned from useTable to build the UI
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page,
    canPreviousPage,
    canNextPage,
    pageOptions,
    pageCount,
    gotoPage,
    nextPage,
    previousPage,
    setPageSize,
    state: {
      pageIndex,
      pageSize,
      sortBy,
      groupBy,
      expanded,
      filters,
      selectedRowIds,
    },
  } = useTable(
    {
      columns,
      data,
      defaultColumn,
      filterTypes,
      // We also need to pass this so the page doesn't change
      // when we edit the data.
      autoResetPage: !skipReset,
      autoResetSelectedRows: !skipReset,
      disableMultiSort: true,
    },
    useFilters,
    useGroupBy,
    useSortBy,
    useExpanded,
    usePagination,
    useRowSelect
  )

  // Render the UI for your table
  return (
    <div className="center">
      <table {...getTableProps()}>
        <thead>
          {headerGroups.map(headerGroup => (
            <tr {...headerGroup.getHeaderGroupProps()}>
              {headerGroup.headers.map(column => (
                <th {...column.getHeaderProps()}>
                  <div>
                    {column.canGroupBy ? (
                      // If the column can be grouped, let's add a toggle
                      <span {...column.getGroupByToggleProps()}>
                        {column.isGrouped ? '🛑 ' : ' '}
                      </span>
                    ) : null}
                    <span {...column.getSortByToggleProps()}>
                      {column.render('Header')}
                      {/* Add a sort direction indicator */}
                      {column.isSorted
                        ? column.isSortedDesc
                          ? ' 🔽'
                          : ' 🔼'
                        : ''}
                    </span>
                  </div>
                  {/* Render the columns filter UI */}
                  <div>{column.canFilter ? column.render('Filter') : null}</div>
                </th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody {...getTableBodyProps()}>
          {page.map(row => {
            prepareRow(row)
            return (
              <tr {...row.getRowProps()}>
                {row.cells.map(cell => {
                  return (
                    <td {...cell.getCellProps()}>
                      {cell.isGrouped ? (
                        // If it's a grouped cell, add an expander and row count
                        <>
                          <span {...row.getToggleRowExpandedProps()}>
                            {row.isExpanded ? '👇' : '👉'}
                          </span>{' '}
                          {cell.render('Cell', { editable: false })} (
                          {row.subRows.length})
                        </>
                      ) : cell.isAggregated ? (
                        // If the cell is aggregated, use the Aggregated
                        // renderer for cell
                        cell.render('Aggregated')
                      ) : cell.isPlaceholder ? null : ( // For cells with repeated values, render null
                        // Otherwise, just render the regular cell
                        cell.render('Cell', { editable: true })
                      )}
                    </td>
                  )
                })}
              </tr>
            )
          })}
        </tbody>
      </table>
      <div className="pagination">
        <button onClick={() => gotoPage(0)} disabled={!canPreviousPage}>
          {'<<'}
        </button>{' '}
        <button onClick={() => previousPage()} disabled={!canPreviousPage}>
          {'<'}
        </button>{' '}
        <button onClick={() => nextPage()} disabled={!canNextPage}>
          {'>'}
        </button>{' '}
        <button onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage}>
          {'>>'}
        </button>{' '}
        <span>
          Page{' '}
          <strong>
            {pageIndex + 1} of {pageOptions.length}
          </strong>{' '}
        </span>
        <span className="margin-right">
          | Go to page:{' '}
          <input
            type="number"
            defaultValue={pageIndex + 1}
            onChange={e => {
              const page = e.target.value ? Number(e.target.value) - 1 : 0
              gotoPage(page)
            }}
            style={{ width: '100px' }}
          />
        </span>{' '}
        <select
          value={pageSize}
          onChange={e => {
            setPageSize(Number(e.target.value))
          }}
        >
          {[10, 20, 30, 40, 50].map(pageSize => (
            <option key={pageSize} value={pageSize}>
              Show {pageSize}
            </option>
          ))}
        </select>
      </div>
    </div>
  )
}

function App() {
  const columns = React.useMemo(
    () => [
      {
        Header: 'Name',
        accessor: 'name'
        // aggregate: 'count',
        // Aggregated: ({ value }) => `${value} Names`,
      },
      {
        Header: 'Item Score',
        accessor: 'item',
        // Use our custom `fuzzyText` filter on this column
        filter: 'fuzzyText'
        // aggregate: 'uniqueCount',
        // Aggregated: ({ value }) => `${value} Unique Names`,
      },
      {
        Header: 'Level Requirement',
        accessor: 'level'
      },
      {
        Header: 'Damage',
        accessor: 'damage'
      },
      {
        Header: 'Accuracy',
        accessor: 'acc'
      },
      {
        Header: 'Handling',
        accessor: 'hand'
      },
      {
        Header: 'Reload Time',
        accessor: 'reload'
      },
      {
        Header: 'Fire Rate',
        accessor: 'fire'
      },
      {
        Header: 'Magazine Size',
        accessor: 'mag'
      },
      {
        Header: 'Price',
        accessor: 'price'
      },
      {
        Header: 'Manufacturer',
        accessor: 'manu'
      }
    ]
  )


  return (
    <div className="App">
      <p style={{ 'backgroundColor': 'yellow', padding: '3em' }}>
        <strong>Items to process are located at: src/assets/items</strong>
      </p>
      <Table
        columns={columns}
        data={JsonData}
      />
    </div>
  )

  // return (
  //   <div className="App">
  //     <p style={{ 'backgroundColor': 'yellow', padding: '3em' }}>
  //       <strong>Items to process are located at: src/assets/items</strong>
  //     </p>

  //     <table>
  //       <caption>Item Table</caption>
  //       <thead>
  //         <tr>
  //           <th>Name</th>
  //           <th>Item Score</th>
  //           <th>Level Requirement</th>
  //           <th>Damage</th>
  //           <th>Accuracy</th>
  //           <th>Handling</th>
  //           <th>Reload Time</th>
  //           <th>Fire Rate</th>
  //           <th>Magazine Size</th>
  //           <th>Price</th>
  //           { /* Located in bottom right corner of item photo */}
  //           <th>Manufacturer</th>
  //         </tr>
  //       </thead>
  //       <tbody>
  //         <tr>
  //           <td>&nbsp;</td>
  //           <td>&nbsp;</td>
  //           <td>&nbsp;</td>
  //           <td>&nbsp;</td>
  //           <td>&nbsp;</td>
  //           <td>&nbsp;</td>
  //           <td>&nbsp;</td>
  //           <td>&nbsp;</td>
  //           <td>&nbsp;</td>
  //           <td>&nbsp;</td>
  //           <td>&nbsp;</td>
  //         </tr>
  //         <tr>
  //           <td>&nbsp;</td>
  //           <td>&nbsp;</td>
  //           <td>&nbsp;</td>
  //           <td>&nbsp;</td>
  //           <td>&nbsp;</td>
  //           <td>&nbsp;</td>
  //           <td>&nbsp;</td>
  //           <td>&nbsp;</td>
  //           <td>&nbsp;</td>
  //           <td>&nbsp;</td>
  //           <td>&nbsp;</td>
  //         </tr>
  //         <tr>
  //           <td>&nbsp;</td>
  //           <td>&nbsp;</td>
  //           <td>&nbsp;</td>
  //           <td>&nbsp;</td>
  //           <td>&nbsp;</td>
  //           <td>&nbsp;</td>
  //           <td>&nbsp;</td>
  //           <td>&nbsp;</td>
  //           <td>&nbsp;</td>
  //           <td>&nbsp;</td>
  //           <td>&nbsp;</td>
  //         </tr>
  //         <tr>
  //           <td>&nbsp;</td>
  //           <td>&nbsp;</td>
  //           <td>&nbsp;</td>
  //           <td>&nbsp;</td>
  //           <td>&nbsp;</td>
  //           <td>&nbsp;</td>
  //           <td>&nbsp;</td>
  //           <td>&nbsp;</td>
  //           <td>&nbsp;</td>
  //           <td>&nbsp;</td>
  //           <td>&nbsp;</td>
  //         </tr>
  //       </tbody>
  //     </table>
  //   </div>
  // );
}

export default App;
