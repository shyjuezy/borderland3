# Syngenta Technical Challenge - Borderlands 3 Scenario

## Background
You are an avid gamer of Borderlands 3, a loot-based multiplayer first person shooter with extra emphasis on the loot. Since you're competitive, you want to have the best loot, which comes in the form of legendary rarity items. 

Because the chance of legendary weapons dropping is low, you realize that trading on forums and Reddit is the best way to get what you want. However, because the process is still relatively manual and involves back and forth messaging of users to explain an item's attributes, you realize you can use OCR to scan images of the items you find in game and extract their attributes programmatically. Later on, you realize it might be good to have a web view of the items you've gotten which shows a picture of the item as well as a formatted view of the item's attributes as a beginning to a future application.

## Requirements
> Note: You can use any frontend and backend technology you want; however, we set up this React frontend to get you started!

1. Extract relavant textual information from images of items.
1. Store textual information in database or managed file, relate text information of the item to its associated image.
1. Create a web viewer (perhaps an HTML table or infinte scroll) that shows the pictures and attributes of items in tabular format.
1. Add sorting and filtering to the table.

## Submission

Send an email containing a link to your repo to tyler.johnston@syngenta.com with subject: `Syngenta Tech Challenge <YOUR NAME>`

## Things to Remember
1. Items to process are located at: `src/assets/items`
1. Find relavant help in HELP.md
1. Be prepared to explain your code, technology choices, how you would scale your solution, and why the choice of technology made since for the piece of functionality developed.
1. Just try to get a working starting point! It doesn't have to be perfect the first time, or look good, it just has to be functional. Divide and conquer the functionality bits first and add looks (if you want to) later.
1. It's better to have a solution that extracts the information from the images as accurately as possible, rather than a quick solution. 
1. It's better to have a working solution that meets fewer requirements than a broken solution that tries to meet the requirements, but can't (since it's broken!).


